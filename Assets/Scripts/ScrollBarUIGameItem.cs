using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ScrollBarUIGameItem : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Text _text;
    private int _count;

    public void SetSprite(Sprite spritePicture)
    {
        _image.sprite = spritePicture;

        if (spritePicture == null)
        {
            throw new ArgumentNullException(nameof(spritePicture), "Sprite is null");     //��������� ������ ��� ��������� ������ � ��������, ��� �������� � throw
        }
        _image.sprite = spritePicture;
    }



    public void SetCount(int count)
    {
        if (count < 0)
        {
            throw new ArgumentException(nameof(count), "count is less zero");
        }
        _count = count;
        _text.text = _count.ToString();
    }
}
