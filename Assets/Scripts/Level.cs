using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Level : MonoBehaviour
{
    private void Awake()
    {
        InitializeLevel();
    }



    [SerializeField] private List<GameItem> _gameItems = new List<GameItem>();

    private int _countOfNotFoundItems;

    public Action OnComplete;



    private void InitializeLevel()
    {
        _countOfNotFoundItems = _gameItems.Count;
        for (int i = 0; i < _gameItems.Count; i++)
        {
            _gameItems[i].ItemWasFound += ItemWasFoundMethod;
        }
    }


    public Dictionary<string, GameItemData> GetItemDictionary()
    {
        Dictionary<string, GameItemData> itemDictionary = new Dictionary<string, GameItemData>();

        for (int i = 0; i < _gameItems.Count; i++)
        {
            if (itemDictionary.ContainsKey(_gameItems[i].Name) == true) // ���� ������� ������� �������� ��� GameItem, ��������� ���� �� ��� ����
            {                                                                       //���� ��� ���� ����� 
                itemDictionary[_gameItems[i].Name].IncreaseCount();
            }
            else
            {
                itemDictionary.Add(_gameItems[i].Name, new GameItemData(_gameItems[i].Sprite)); // ������ ����� ������ � ���� ���� ����� �������� ��� ��� �� ����
            }
        }

        return itemDictionary;     //��������� ������ ��� gameItem-�, ���.
    }
    // ��� ��������, ������� ��������� ���� ������ ��� ������ ���� ������

    private void ItemWasFoundMethod()
    {
        _countOfNotFoundItems--;

        if (_countOfNotFoundItems == 0)
        {
            Debug.Log("level completed");
            OnComplete?.Invoke();
        }
    }    
}
