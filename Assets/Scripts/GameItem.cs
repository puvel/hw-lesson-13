using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]

public class GameItem : MonoBehaviour   //����� ������� ��������� ���� ������� ������ ������������ ��������
{
    private void Awake()
    {
        visualMethod = GetComponent<OnFoundVisualMethod>();

        _spriteRenderer = GetComponent<SpriteRenderer>();
    }


    [SerializeField] private string _name;
    public string Name { get { return _name; } }

    private SpriteRenderer _spriteRenderer;
    public Sprite Sprite { get { return _spriteRenderer.sprite; } }


    public Action ItemWasFound;
    private OnFoundVisualMethod visualMethod;


    private void OnMouseUpAsButton()
    {
        // � ��� ������ ��� ��� ������
        visualMethod.ShowEffect();
        Debug.Log("You found the item!");
        ItemWasFound?.Invoke();

    }
}


