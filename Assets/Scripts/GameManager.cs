using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels = new List<Level>();
    [SerializeField] private UiManager _uiManager;
    private int _completedLevelsCounter;

    private Level _currentLevel;
    private Level _currentLevelScript;



    private void Start()
    {
        _uiManager.ShowStartScreen();
    }



    public void CreateNewLevel()
    {
        if (_currentLevel != null)
        {
            Destroy(_currentLevel.gameObject);
            _currentLevel = null;
        }

        _currentLevel = _levels[_completedLevelsCounter];
        _currentLevel = Instantiate(_currentLevel.gameObject).GetComponent<Level>();
        _currentLevel.OnComplete += _levelCompletedMethod;
        _uiManager.ShowGameScreen(_currentLevel.GetItemDictionary()); // ������������ ���� ��� ���� ��������

    }


    private void _levelCompletedMethod()
    {
        _completedLevelsCounter++;
        if (_completedLevelsCounter == _levels.Count)         //������������
        {
            Debug.Log("��� ������ ��������, �� ��������");
            _uiManager.ShowStartScreen();
            _completedLevelsCounter = 0;
        }
        else
        {
            CreateNewLevel();
            _uiManager.ShowWinScreen();
        }
    }

    public void StartGame()
    {
        CreateNewLevel();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}


// �� ��������� ���� �� ����. �������