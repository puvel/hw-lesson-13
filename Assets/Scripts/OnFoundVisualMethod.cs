using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OnFoundVisualMethod : MonoBehaviour
{

    public virtual void ShowEffect()
    {
        Debug.Log("Something in Vis is happening");

    }
}
