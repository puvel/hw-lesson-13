using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameScreen : MonoBehaviour
{
    // ���������� ������� � ���� ���������

    [SerializeField] private Transform _contentTransform;
    [SerializeField] private GameObject _uiItemPrefab;


    private Dictionary<string, ScrollBarUIGameItem> _uiItems = new Dictionary<string, ScrollBarUIGameItem> ();

    public void Initialize(Dictionary<string, GameItemData> itemsData)
    {
        foreach(var key in _uiItems.Keys)
        {
            Destroy(_uiItems[key].gameObject);
        }

        GenerateUIItems(itemsData);
    }

    private void GenerateUIItems(Dictionary<string, GameItemData> ItemsData)
    {

        Debug.Log($"Dictionary - {ItemsData.Keys.Count}");
        foreach (var key in ItemsData.Keys)
        {
            GameObject uiGameItem = Instantiate(_uiItemPrefab, _contentTransform);
            ScrollBarUIGameItem gameItemComponent = uiGameItem.GetComponent<ScrollBarUIGameItem>();

            gameItemComponent.SetSprite(ItemsData[key].Sprite);
            gameItemComponent.SetCount(ItemsData[key].Count);

            _uiItems.Add(key, gameItemComponent);
        }
    }
}
