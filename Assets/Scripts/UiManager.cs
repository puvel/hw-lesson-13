using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    [SerializeField] private GameObject _startScreen;
    [SerializeField] private GameObject _winScreen;
    [SerializeField] private GameObject _gameScreen;

    private GameObject _currentScreen;

    private UIGameScreen _uiGameScreen; // �������� ������� ����� �������� �����������


    private void Awake()
    {
        _currentScreen = _startScreen;

        _startScreen.SetActive(false);
        _gameScreen.SetActive(false);
        _winScreen.SetActive(false);

        _uiGameScreen = _gameScreen.GetComponent<UIGameScreen>();
    }

    public void ShowStartScreen()
    {
        _currentScreen.SetActive(false);
        _currentScreen = _startScreen;
        _currentScreen.SetActive(true);
    }

    public void ShowGameScreen(Dictionary<string, GameItemData> itemData)
    {
        
        _currentScreen.SetActive(false);
        _currentScreen = _gameScreen;
        _uiGameScreen.Initialize(itemData);
        _currentScreen.SetActive(true);
    }

    public void ShowWinScreen()
    {
        _currentScreen.SetActive(false);
        _currentScreen = _winScreen;
        _currentScreen.SetActive(true);
    }
}
