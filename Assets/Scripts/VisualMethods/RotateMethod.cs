using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateMethod : OnFoundVisualMethod
{
    [SerializeField] private float _endValue;
    [SerializeField] private float _duration;
    SpriteRenderer _spriteRenderer;
                                        
    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();                                       
    }
    public override void ShowEffect()
    {
        base.ShowEffect();
        transform.DORotate(new Vector3(0f, 0f, _endValue), _duration, RotateMode.FastBeyond360).OnComplete(() =>
        {
            _spriteRenderer.enabled = false;   //�������������� ������?
        });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    }
}
