using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DisableMethod : OnFoundVisualMethod
{
    [SerializeField] private float _endValue;
    [SerializeField] private float _duration;
    SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public override void ShowEffect()
    {
        base.ShowEffect();

        _spriteRenderer.DOFade(_endValue, _duration).OnComplete( () =>
        {
            gameObject.SetActive(false);
        });

    }
}
